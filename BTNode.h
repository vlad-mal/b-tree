//
// Created by mv on 24.04.2019.
//

#ifndef PZ_MVV_B_TREE_BTNODE_H
#define PZ_MVV_B_TREE_BTNODE_H

#include <vector>
#include <algorithm>
#include <sstream>
#include <iostream>
#include "BTree.h"

using namespace std;

template<class TKey, int t>
class BTNode
{
public:

    ~BTNode()
    {
        for (auto child :childs)
            delete child;
    };// Рекурсивно убиваем все подузлы

    void insert(TKey key);
    bool del(TKey key);

    bool isLeaf()
    {
        return !childs.size();
    };

    vector<TKey> keys;
    vector<BTNode<TKey, t> *> childs;

    void split_child(size_t idx);

    void traverse(); // Выводит все ключи в порядке возрастания
    void showkeys(string &leftspace, int level, bool withendl);

private:
    void delFromLeaf(size_t idx);
    void delFromNonLeaf(size_t idx);
    void mergeChilds(size_t idx);
    TKey getPrevKey(size_t idx);
    TKey getSuccKey(size_t idx);
    void fillNode(size_t idx);
    void pickFromPrev(size_t idx);
    void pickFromSucc(size_t idx);

};

template<class TKey, int t>
void BTNode<TKey, t>::split_child(size_t idx)   // Разбивает дочерний узел childs[idx] на два (левый и правый) и меняет
{
    // поля this

    BTNode<TKey, t> *left = childs[idx]; // После разбиения исходный узел будет левым, потом его укоротим
    BTNode<TKey, t> *right = new BTNode<TKey, t>; // Правый узел создаем и копируем из левого хвост

    right->keys.assign(left->keys.begin() + t, left->keys.end()); // Копируем из левого
    if (!left->isLeaf())
        right->childs.assign(left->childs.begin() + t, left->childs.end()); // "правые" ключи и указатели

    keys.insert(keys.begin() + idx, left->keys[t - 1]); // Вставляем медианный ключ в позицию idx.
    childs.insert(childs.begin() + idx + 1, right);

    left->keys.resize(t - 1);
    if (!left->isLeaf())
        left->childs.resize(t);
}


template<class TKey, int t>
void BTNode<TKey, t>::delFromLeaf(size_t idx) // Из листа тупо удаляем
{
    keys.erase(keys.begin()+idx);
}


template<class TKey, int t>
void BTNode<TKey, t>:: mergeChilds(size_t idx)
{
    // Сливаем деток ключа, лежащего в позиции idx, в один:
    // К левому (childs[idx]) перемещаем сам ключа keys[idx] и весь правый (childs[idx+1}) - ключи и ссылки на детей.
    // Потом правый (childs[idx+1]) удаляется.
    BTNode<TKey, t> * left = childs[idx];
    BTNode<TKey, t> * right = childs[idx+1];

    left->keys.push_back(keys[idx]); // Добавили сам ключик
    left->keys.insert(left->keys.end(), right->keys.begin(), right->keys.end()) ;// Добавили все ключи из правого
    left->childs.insert(left->childs.end(), right->childs.begin(), right->childs.end());// Добавили все ссылки на деток правого

    keys.erase(keys.begin() +idx); // Удаляем ключ, т.к. он был перемещен в left
    childs.erase(childs.begin() +idx+1); // ... а также ссылку на right

    right->childs.clear(); // Чтобы при удалении rights в деструкторе не удалялись детки
    delete right;

}


template<class TKey, int t>
void BTNode<TKey, t>::delFromNonLeaf(size_t idx) // Не лист. Чуть сложнее
{
    // Если слева как минимум t ключей - удаляем из левого поддерева. Иначе, если справа не меньше t - удаляем из правого.
// А если и там и там меньше, чем t, то соединяем оба поддерева и удаляем из результата слияния:
    auto keyToDelete = keys[idx];

    if(childs[idx]->keys.size() >= t)  // Левое поддерево
    {
        // Если ребенок слева от keyToDelete (childs[idx]) имеет как минимум t ключей,
        // работаем с левым поддеревом childs[idx]:
        // находим ключ keyPred, предшествующий ключу keyToDelete в этом левом поддерева (childs[idx]),
        // (бежим по правой грани левого поддерева до самого листа - там и будет keyPred).
        // Заменяем keyToDelete на найденного предшественника keyPred. Затем рекурсивно удаляем этого
        // найденного предшественника keyPred в левом поддереве (childs[idx]).

        auto keyPred =  getPrevKey(idx);
        keys[idx] = keyPred;
        childs[idx]->del(keyPred);
    }
    else if(childs[idx+1]->keys.size() >= t)  // Правое поддерево
    {
        // Если ребенок справа от keyToDelete (childs[idx+1]) имеет как минимум t ключей,
        // работаем с левым поддеревом childs[idx+1]:
        // находим ключ keySucc, следующий за ключом keyToDelete в этом правом поддерева (childs[idx+1]),
        // (бежим по левой грани правого поддерева до самого листа - там и будет keySucc).
        // Заменяем keyToDelete на найденный keySucc. Затем рекурсивно удаляем этого
        // найденного keySucc в правом поддереве (childs[idx+]).

        auto keySucc =  getSuccKey(idx);
        keys[idx] = keySucc;
        childs[idx + 1]->del(keySucc);
    }
    else    // Оба ребенка (childs[idx] и childs[idx+1]) меньше, чем t,
        // добавляем в левого (childs[idx]) ключ keyToDelete и все из правого (childs[idx+1]).
        // Правого (childs[idx+1]) стираем. Теперь в childs[idx] точно 2t-1 ключей.
        // Рекурсивно удаляем ключ keyToDelete из childs[idx].
    {
        mergeChilds(idx);
        childs[idx]->del(keyToDelete);
    }

};

template<class TKey, int t>
bool BTNode<TKey, t>::del(TKey key)
{
    // А есть ли именно в этом узле ключ key?

    auto place = lower_bound(keys.begin(), keys.end(), key); // Ищем первый ключ, которыей не меньше, чем key
    size_t idx = place - keys.begin(); // idx - его индекс
    if ((idx >= 0)&& (idx < keys.size()) && (keys[idx] == key)) //Да, ключ key в этом узле присутствует
    {
        if (isLeaf())
            delFromLeaf(idx);
        else
            delFromNonLeaf(idx);
        return true;
    }
    else // Ключа key в узле нетъ. Надо что-то делать!
    {
        if (isLeaf())
            return false; // Значит, нет его нигде.

        // Может быть, ключ лежит есть где-то ниже?
        // В этой точке idx равен либо 0, либо keys.size().
        // Таким образом, childs[idx] указывает либо на левое дерево, если ключ меньше всех ключей, либо на
        // правое, если ключ больше всех ключей. Куда указывает, с тем и будем работать: может, ключ именно там.

        if (childs[idx]->keys.size() < t) // Удаляем только из нодв, где число ключей не меньше, чем t
            fillNode(idx); // Дополняем нод (берем у толстых соседей, или просто сливаемся с худым соседом)


        if (idx > keys.size())
            return
                childs[idx-1]->del(key);
        else
            return
                childs[idx]->del(key);

    }




}

template<class TKey, int t>
void BTNode<TKey, t>::traverse()
{
    // Всего keys.size() ключей и +1 детей, показываем ключи
    // и первые keys.size() детей
    int i;
    for (i = 0; i < keys.size(); i++)
    {
        // Если это не лист, то перед печатью ключа key[i]
        // печатать поддерева с корнем childs[i].
        if (!isLeaf())
            childs[i]->traverse();
        cout << " " << keys[i];
    }

    // Печать поддерева с корнем в последнем ребенке
    if (!isLeaf())
        childs[i]->traverse();
}

template<class TKey, int t>
void BTNode<TKey, t>::insert(TKey key)   // Выполняет вставку ключа key в незаполненный узел this.
{
    if (isLeaf())   // Если это лист, то просто вставляем в нужное место ключ key:
    {
        auto newplace = upper_bound(keys.begin(), keys.end(), key); // Находим место, где ключи больше, чем key
        keys.insert(newplace, key); // Вставляем перед ним key
    }
    else     // Не лист, ищем место перехода на нижний уровень
    {
        auto newplace = upper_bound(keys.begin(), keys.end(), key); // Находим место, где ключи больше, чем key
        auto newidx = newplace - keys.begin();
        auto current = childs[newidx]; // В этого детку будем вставлять
        if (current->keys.size() == 2 * t - 1)   // А в нем места нет - разбиваем на два
        {
            split_child(newidx); // Разбиваем на 2, current - это левая часть. В keys[newidx] - медиана из старого узла
            if (key > keys[newidx]) // Если ключ больше, чем медиана,
                current = childs[newidx + 1]; // то работаем с правой половинкой
        }
        void fillNode(int idx);
        void pickFromPrev(int idx);
        void pickFromSucc(int idx);

        current->insert(key); // Рекурсия , сцобако.
    }
}

template<class TKey, int t>
TKey BTNode<TKey, t>::getPrevKey(size_t idx)
{
    // Возвращает ключ, предшествующий ключу key[idx]
    // Движемся вниз, начиная с левого потомка (childs[idx]), придерживаясь правого края, пока не достигнем листа
    BTNode<TKey, t> *nextNode=childs[idx];
    while (!nextNode->isLeaf())
        nextNode = nextNode->childs[nextNode->keys.size()];

    // Возвращаем последний (самый правый) ключ листа
    return nextNode->keys[nextNode->keys.size()-1];

}
template<class TKey, int t>
TKey BTNode<TKey, t>::getSuccKey(size_t idx)
{
    // Возвращает ключ, слудующий за ключом key[idx]

    // Движемся вниз, начиная от правого потомка (childs[idx+1]), придерживаясь левой стороны, пока не достигнем листа
    BTNode<TKey, t>*nextNode = childs[idx+1];
    while (!nextNode->isLeaf())
        nextNode = nextNode->childs[0];

    // Возвращаем первый (самый левый) ключ листа
    return nextNode->keys[0];
}

template<class TKey, int t>
void BTNode<TKey, t>:: fillNode(size_t idx)
{
    // Ключей слишком мало (меньше, чем t-1), добавляем:

    // Если у левого (предыдущего) ребенка (childs[idx-1]) ключей больше, чем t-1, забираем у него
    if (idx && (childs[idx-1]->keys.size() >= t))
        pickFromPrev(idx);

    // Иначе, если у правого (следующего) ребенка (childs[idx+1]) ключей больше, чем t-1 keys, забираем у него
    else if (idx!= keys.size() && childs[idx+1]->keys.size()>=t)
        pickFromSucc(idx);

    // Ой, ё, ключей мало и у левого и у правого, поеэтому сливаем их в один
    // Если childs[idx] является последним ребенком, сливаем его с предыдущим (левым)
    // Иначе сливаем его с последующим (правым)
    else
    {
        if (idx == keys.size())
            mergeChilds(idx-1); // idx - последний, сливаем предыдущий с ним
        else
            mergeChilds(idx); // сливаем с последующим
    }
    return;

}

template<class TKey, int t>
void BTNode<TKey, t>:: pickFromPrev(size_t idx)
// Вставляем ключ в childs[idx], заняв его у брата слева (childs[idx-1])
{
    BTNode<TKey, t> *me=childs[idx]; // Куда вставляем ключ
    BTNode<TKey, t> *leftBrother=childs[idx-1]; // У кого забираем ключ

    // Последний (правый) ключ из leftBrother перемещается вверх, к родителю, а ключ keys[idx-1]
    // родителя вставляется первым в me. Таким образом, "братик" теряет один ключ, а me приобретает

    // Moving all key in childs[idx] one step ahead

    me->keys.insert(me->keys.begin(), keys[idx+1]);
    if (!me->isLeaf())
        me->childs.insert(me->childs.begin(), leftBrother->childs[leftBrother->keys.size()]);

    keys[idx-1] = leftBrother->keys[leftBrother->keys.size()-1];

    leftBrother->keys.resize(leftBrother->keys.size() - 1);
    if (!leftBrother->isLeaf())
        leftBrother->childs.resize(leftBrother->childs.size() - 1);

}

template<class TKey, int t>
void BTNode<TKey, t>:: pickFromSucc(size_t idx)
// Добавляет в конец childs[idx] ключ, забрав его у брата справа (childs[idx+1])
{

    BTNode<TKey, t> * me=childs[idx];
    BTNode<TKey, t> * rightBrother=childs[idx+1];

    // Добавляем в конец списка ключей me ключ из keys[idx] (родительски)
    me->keys.push_back(keys[idx]);

    // Добавляем в конец списка детей me первого ребенка братика
    if (!me->isLeaf())
        me->childs.push_back(rightBrother->childs[0]);

    //The first key from sibling is inserted into keys[idx]
    keys[idx] = rightBrother->keys[0];

    rightBrother->keys.erase(rightBrother->keys.begin());
    if (!rightBrother->isLeaf())
        rightBrother->childs.erase(rightBrother->childs.begin());
}



template<class TKey, int t>
void BTNode<TKey, t>::showkeys(string &leftspace, int level, bool withendl)
{

    for (size_t i = 0; i < childs.size(); i++)
        childs[i]->showkeys(leftspace, level -1, withendl && (i == (childs.size() - 1)));

    if (!level )
        cout << leftspace <<" ";


    for (size_t i = 0; i < keys.size(); i++)
    {
        string delta = "";
        delta = to_string(keys[i]);
        if ((keys.size()>1)&&(i<(keys.size()-1)))
            delta += ",";
        else
            delta += " ";


        if (!level )
        {
            cout << delta;
            if (i==(keys.size()-1))
                if (withendl)
                    cout << endl;
        }
        else
            leftspace += string(delta.size() /2, ' ');

    }


}
#endif //PZ_MVV_B_TREE_BTNODE_H
