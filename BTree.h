//
// Created by mv on 28.04.2019.
//

#ifndef PZ_MVV_B_TREE_BTREE_H
#define PZ_MVV_B_TREE_BTREE_H

#include "BTNode.h"

// B - дерево. Тип ключа TKey, минимальная степень (minimal_degree) t.
// Min число ключей в любом (кроме корневого) узле: t-1, максимальное: 2t-1.
// Каждый узел содержит не более 2t - 1 ключей.
// Изначально дерево не содержит ни одного узла.
template<class TKey, int t>
class BTree
{
public:
    ~BTree()
    {
        if (root)
            delete root;
    };

    void insert(TKey key);
    void insert(const vector<TKey> &keys); // Добавление массива ключей, например: insert({1, 2, 3, 4, 5});
    bool exist(TKey key);
    bool search(TKey key, BTNode<TKey, t> *&node, int &idx); // Ищет ключ key, возвращает true, узел и индекс ключа.
    bool del(TKey key); // Удаление, true - если такой ключ был. Результат полезен для удаления в цикле, если есть несколько одинаковых ключей
    void traverse();
    size_t height();
    void show();
    BTNode<TKey, t> *root = nullptr;

};

template<class TKey, int t>
void BTree<TKey, t>::traverse()
{
    if (root)
        root->traverse();
}

// Вычисление высоты дерева, включая корень и листья
template<class TKey, int t>
size_t BTree<TKey, t>::height()
{
    size_t rez = 0;
    BTNode<TKey, t> *next = root;

    if (next)
        for (;;)
        {
            rez++;
            if (next->isLeaf())
                break;
            next = next->childs[0];
        }

    return rez;
}

template<class TKey, int t>
void BTree<TKey, t>::insert(TKey key)
{
    if (!root) // При первом добавлении ключа создаем корень.
        root = new BTNode<TKey, t>;

    BTNode<TKey, t> *current = root;

    if (current->keys.size() == 2 * t - 1)   // Корневой узел заполнен, дерево растет вверх
    {
        root = new BTNode<TKey, t>; // Корень теперь - новый незаполненный узел
        root->childs.push_back(current);
        root->split_child(0);
        root->insert(key);
    }
    else
        current->insert(key);
}


template<class TKey, int t>
bool BTree<TKey, t>::exist(TKey key)   // Ищет ключ key в B-дереве
{
    if (!root)
        return false;
    auto current = root;
    for (;;)
    {
        // Так как все ключи упорядочены, используем двоичный поиск
        if (binary_search(current->keys.begin(), current->keys.end(), key))
            return true;
        if (current->isLeaf()) // Не найдено. :(
            return false;
        auto place = upper_bound(current->keys.begin(), current->keys.end(), key);
        current = current->childs[place - current->keys.begin()];
    }
}


template<class TKey, int t>
void BTree<TKey, t>::insert(const vector<TKey> &keys)
{
    for (auto key : keys)
        insert(key);
}

template<class TKey, int t>
bool BTree<TKey, t>::del(TKey key)
{
    if (!root)
        return false;
    if (!root->del(key))
        return false;
    // If the root node has 0 keys, make its first child as the new root
    //  if it has a child, otherwise set root as NULL
    if (!root->keys.size())
    {

         BTNode<TKey, t> *tmp = root;
        if (root->isLeaf())
            root = NULL;
        else
            root = root->childs[0];

        // Удаляем старый корешок
        tmp->childs.clear(); // Чтобы не выстрелило в деструкторе удаление на деток
        delete tmp;
    }
    return true;
}

template<class TKey, int t>
bool BTree<TKey, t>::search(TKey key, BTNode<TKey, t> *&node, int &idx)
{
    if (!root) // Пустое дерево
        return false;
    auto current = root;
    for (;;)
    {
        // Так как все ключи упорядочены, используем двоичный поиск
        // Находим место, начиная с которого ключи больше, чем key
        auto place = upper_bound(current->keys.begin(), current->keys.end(), key);
        if (place > current->keys.begin())// Результат может указывать и на keys.end(), т.е. не нашли.
            if (* (place - 1) == key)     // Поэтому проверяем, что предыдущий ключ - это именно key.
            {
                node = current;
                idx = place - current->keys.begin() - 1;
                return true;
            }
        if (current->isLeaf()) // Искать больше негде
            return false;
        current = current->childs[place - current->keys.begin()]; // Ищем в поддереве, где ключи не меньше, чем key
    }
}


template<class TKey, int t>
void BTree<TKey, t>::show()
{
    if (! root)
        return;
   cout << endl;
    for (size_t level = 0; level < height(); level++)
    {
        string leftspace = "";
        root->showkeys(leftspace, level, true);
    }
}

#endif //PZ_MVV_B_TREE_BTREE_H
