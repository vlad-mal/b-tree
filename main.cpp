#include <iostream>
#include "BTree.h"

using namespace std;
const int min_degree = 3;

template<class TKey, int t>
void del(BTree<TKey, t>  * tree, TKey val){
    tree->del(val);
    cout << endl << "deleted " << val << endl;
    tree->traverse();
    cout << endl << "Tree:" << endl;
    tree->show();

}
int main()
{
    BTree<int, min_degree> tree;

    cout << endl << "Tree height (including root and leaves) = " << tree.height() << endl;

    // tree.insert({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
    tree.insert({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20});
    // tree.insert({1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1});
    /*
        tree.insert(1);
        tree.insert(2);
        tree.insert(3);
        tree.insert(4);
        tree.insert(5);
        tree.insert(6);
    */

    tree.traverse();
    BTNode<int, min_degree> * node;
    int idx;
    tree.search(2, node, idx);
    tree.search(122, node, idx);

    cout << endl << "Tree height (including root and leaves) = " << tree.height() << endl;

    cout << tree.exist(3);
    cout << tree.exist(22);
    cout << endl;
    tree.traverse();
    cout << endl;
    tree.show();

    del(&tree, 3);
    del(&tree, 5);
    del(&tree, 4);
    del(&tree, 6);


    return 0;
}
