//#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "catch.hpp"

#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <queue>

#include "BTree.h"



TEST_CASE( "Check height", "[B-Tree]" )
{
    srand(time(NULL));
    BTree<int, 2> * tree = new BTree<int, 2>;

    REQUIRE( tree->height() == 0 );

    tree->insert(1);// 1
    REQUIRE( tree->height() == 1 );

    tree->insert(2);//1,2
    REQUIRE( tree->height() == 1 );

    tree->insert(3);//1,2,3
    REQUIRE( tree->height() == 1 );

    tree->insert(4);
    //tree.show();
    //  2
    // 1 3,4
    REQUIRE( tree->height() == 2 );

    tree->insert({5, 6, 7,8,9, 10, 11, 12});
//    tree.show();
//     4
//  2     6,8,10
// 1 3  5  7 9  11,12
    REQUIRE( tree->height() == 3 );
//================
    delete tree;
//================
// ������ - ����:
    tree = new BTree<int, 2>;
    int len = 100 + 10 * rand();
    size_t minHeight = 0;
    size_t maxHeight = 0;
    for (auto i = 0; i< len; i++)
    {
        //https://en.wikipedia.org/wiki/B-tree#Best_case_and_worst_case_heights
        if (i > 0)
        {
            // |LOGm(n+1)| - 1; �������� �� ��������� m (������������ ����� ����� = 2t) �� (����� ������ +1), ����� 1.
            minHeight = abs(log(i+1)/log(2*2)) -1 ;
            // |LOGd (n+1)/2|; �������� �� ��������� d (����������� ����� ����� � ��-����� = t-1)
            maxHeight = abs(log((i+1)/2)/log(2));
        }
        REQUIRE( tree->height() >= minHeight); // � ��� ������ ��������� 1, 2, 3... ������ � root � ��������
        REQUIRE( tree->height() <= maxHeight+1);

        tree->insert(rand());

    }
    delete tree;

}

TEST_CASE( "Check exist()", "[B-Tree]" )
{
    BTree<int, 2> * tree = new BTree<int, 2>;
    REQUIRE_FALSE(tree->exist(123));
    tree->insert(123);
    REQUIRE_FALSE(tree->exist(1));
    REQUIRE(tree->exist(123));
    tree->insert({1, 2, 3, 4,5,6});
    REQUIRE_FALSE(tree->exist(111));
    REQUIRE(tree->exist(6));
    delete tree;
}

TEST_CASE( "Check search()", "[B-Tree]" )
{
    BTree<int, 2> tree;
    BTNode<int, 2> * node;
    int idx;
    REQUIRE_FALSE(tree.search(123, node, idx));
    tree.insert({1, 2,3});

    REQUIRE(tree.search(2, node, idx));
    REQUIRE(idx == 1);
    REQUIRE(node->keys[idx] == 2);
    REQUIRE(node->keys.size() == 3);
    REQUIRE(node->childs.size() == 0);
    REQUIRE(node->isLeaf());
}

TEST_CASE( "Check del()", "[B-Tree]" )
{
    BTree<int, 2> tree;

    REQUIRE_FALSE(tree.del(123));

    tree.insert({1, 2,3});

    REQUIRE(tree.exist(3));
    REQUIRE(tree.del(3));

    REQUIRE_FALSE(tree.exist(3));

    tree.insert({3, 3,3, 3});
    for (int i = 0; i < 4; i++)
    {
        REQUIRE(tree.exist(3));
        REQUIRE(tree.del(3));
    }

    REQUIRE_FALSE(tree.del(3));
    REQUIRE_FALSE(tree.exist(3));


    tree.insert({3, 3, 3, 3, 3, 3, 3, 3});

    REQUIRE(tree.exist(3));
    tree.del(3);
    REQUIRE(tree.exist(3));
    while (tree.del(3));
    REQUIRE_FALSE(tree.exist(3));
}
template<class TKey, int t>
void CheckChildsSizeAndLeavesDepth(BTNode<TKey, t> * node, const size_t sampleHeight, int currDept)
{
    currDept++;
    for(auto child : node->childs)
    {

        REQUIRE(child->keys.size() >= 2-1); //t-1
        REQUIRE(child->keys.size() <= 3); //2t-1
        if (!child->isLeaf())
            CheckChildsSizeAndLeavesDepth(child, sampleHeight, currDept);
        else
            REQUIRE(sampleHeight - 1 == currDept);
    }
}


template<class TKey, int t>
void checkKeysAscending(BTNode<TKey, t> * node)
{ // ���������, ��� ���� � ���� ����������� � ����������� �������,
    // � ����� ��, ��� ���� ������� ����� �� ������ �������� �����,
    // � ������� ����� - �� ������ ��������

    // ����� keys.size() ������ � +1 �����, ���������� �����
    // � ������ keys.size() �����
    int i;
    for (i = 0; i < node->keys.size(); i++)
    {
        // ���� ��� �� ����, �� ����� ������� ����� key[i]
        // �������� ��������� � ������ childs[i].
        if (!node->isLeaf())
        {
            checkKeysAscending(node->childs[i]);
            // ��������� ���� ������ ������� �� ������ �������� �����
            REQUIRE( node->childs[i]->keys[node->childs[i]->keys.size() - 1]
                     <= node->keys[i]);
            // ������ ���� ������� ������� �� ������ �������� �����
            REQUIRE( node->childs[i+1]->keys[node->childs[i+1]->keys.size() - 1]
                     >= node->keys[i]);
        }
        if (i > 0)
            REQUIRE(node->keys[i - 1] <= node->keys[i]);
    } // � ���� ����� i ����� keys.size(), �.�., childs[i] ������� �� ���������� �������

    // ������ ��������� � ������ � ��������� �������
    if (!node->isLeaf())
        checkKeysAscending(node->childs[i]);
}

TEST_CASE( "Check structure()", "[B-Tree]" )
{
    BTree<int, 2> tree; // ������

    tree.insert({0,1,2}); // ��� ���������� � �����
    // tree.show(); - ����� ���������
    REQUIRE(tree.root->isLeaf()); // ������ - ��� � ����
    for(size_t i = 0; i < 3; i++)
        REQUIRE(tree.root->keys[i] == i); // ����� ��������� � ���������

    tree.insert({0,1,2}); // ��� ������� �� ���������
//    tree.show(); - ����� ��������, ��������� ��� ���:
//     1
// 0,0   1,2,2
    BTNode<int, 2> * root = tree.root;

    REQUIRE(root->keys.size() == 1); // � ������� ���� ����
    REQUIRE(!root->isLeaf()); // ������� - �� ����
    for (auto child : root->childs)
    {
        REQUIRE(child->isLeaf()); // ��� ����� - ������
        REQUIRE(child->keys.size() >= 2-1); //t-1 - ������� ������������������ �����������
    }

    CheckChildsSizeAndLeavesDepth(root, 2, 0); // �������� ������� ����� � ������� �������
    for (size_t i = 0; i < 1000; i++ ) // ���� ���������� ����. ����� ������� ���������� ����������� ��� ����
    {
        tree.insert(rand());
        root = tree.root;  // ���� � ���, ��� ����� ������� ������ ����� ����� ������ ����
        size_t sampleHeight = tree.height(); // ���������� �������
        CheckChildsSizeAndLeavesDepth(root, sampleHeight, 0); // �������� ����� �� ������ � �� ��, ��� ��� ������ �� ����� �������
        checkKeysAscending(root); // �������� ����� ������ �� ������������� ������, ����� �������
    }
}
TEST_CASE( "That's all!", "[B-Tree]" ){
		cout << endl << "That's all!.\n  (\\__/) \n (='.'=) \n('')_('')\n" << endl;
}

